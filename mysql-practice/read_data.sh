#!/bin/bash

USER="root"
PASS="123123"

depts=`mysql -u $USER -p$PASS student <<EOF |tail -n +2
SELECT DISTINCT dept from students;
EOF`

for d in $depts
do 
echo "   department : $d"
	result="`mysql -u $USER -p$PASS student <<EOF
set @i:=0;
select @i:=@i+1 as rank,name,mark from students where dept="$d" order by mark desc;
EOF`"

	echo "$result"
	echo 
done
