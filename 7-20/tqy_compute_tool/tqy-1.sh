#!/bin/bash
# 加减乘除

# 加
add(){
	read -p "please input 2 number:" num1 num2
	result=$(echo "${num1}+${num2}" | bc)
	echo "${num1}+${num2}=${result}"
}
# 减
sub(){
	read -p "please input 2 number:" num1 num2
	result=$(echo "${num1}-${num2}" | bc)
	echo "${num1}-${num2}=${result}"
}
# 乘
multi(){
	read -p "please input 2 number:" num1 num2
	result=$(echo "${num1}*${num2}" | bc)
	echo "${num1}*${num2}=${result}"
}
# 除
div(){
	read -p "please input 2 number:" num1 num2
	result=$(echo "${num1} ${num2}" | awk '{print $1/$2}')
	echo "${num1}/${num2}=${result}"
}
# 比较
cmp(){
	read -p "please input 2 number:" num1 num2
	if (( $(echo "${num1}>${num2}" | bc) == 1 )); then
		echo "${num1} > ${num2}"
	elif (( $(echo "${num1}<${num2}" | bc) == 1 )); then
		echo "${num1} < ${num2}"
	else	
		echo "${num1} = ${num2}"
	fi
}

menu(){
	echo "1.add"
	echo "2.minus"
	echo "3.multiplication"
	echo "4.division"
	echo "5.compare"
	echo "6.exit"
	read -p "please choose:" choice
}

main(){
while :
do
	echo '####################'
	menu
	case $choice in
	  1)
		add ;;
	  2)
		sub ;;
	  3)
		multi ;;
	  4)
		div  ;;
	  5)
		cmp  ;;
	  6)
		exit ;;
	  *)
		exit ;;
	esac
done
}

main
