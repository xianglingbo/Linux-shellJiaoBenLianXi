#!/bin/bash
for i in {1..254}
do
	(ping -c 1 -i 0.2 -w 1 192.168.0.$i &>/dev/null
	if (($?==0)) ;then
		echo "192.168.0.$i" >>up.txt
	else 
		echo "192.168.0.$i" >>down.txt
	fi)&
done
sleep 2 
up=`cat up.txt`
down=`cat down.txt`
up_total=`cat up.txt |wc -l`
down_total=`cat down.txt |wc -l`
echo "========================================"
echo "ping up pc total ${up_total}"
echo "${up}"
echo "========================================"
echo "ping down pc total ${down_total}"
echo "${down}"
