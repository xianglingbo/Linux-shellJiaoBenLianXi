#!/bin/bash

name="hello"
myname0='my name is $name'
myname1="my name is '$name'"
myname2='my name is "$name"'
myname3="my $name is "$name""
myname4='my $name is '$name''

echo $myname0
echo $myname1
echo $myname2
echo $myname3
echo $myname4

