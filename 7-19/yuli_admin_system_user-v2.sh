#!/bin/bash
#require: show a system user admin Menu and give some options to add or del users
#Author :yuli
#Date :2014-5-14
##############################################################################
#menu
menu(){
 echo -e "\033[45;37m\t\t****** ***** system user admin Menu ***** ******\033[0m"
 echo -e "\033[46;37m\t\t\t1. add user\033[0m"
 echo -e "\033[42;37m\t\t\t2. del user\033[0m"
 echo -e "\033[43;37m\t\t\t3. query user information\033[0m"
 echo -e "\033[45;37m\t\t\t4. reset user passwd\033[0m"
 echo -e "\033[43;37m\t\t\t5. modify user information\033[0m "
 echo -e "\033[42;37m\t\t\t6. show all user information\033[0m"
 echo -e "\033[46;37m\t\t\t7. exit the program\033[0m"

}
#############################################################################
#add user 
add_user(){
  read -p "^^^^^please input the user name:"  name
  read -sp "^^^^^please input the user password:"  password
  echo -e "\t"
  echo "$password" |/usr/bin/passwd --stdin $name &>/dev/null
  /usr/sbin/useradd $name
  if (( $? == 0 ))
    then
    echo -e "\033[31m @^@  Congratulation! Add user success !  @^@\033[0m"
  else
    echo -e "\033[31m *_* Sorry ,Add user failure ! *_* \033[0m"
  fi  
}
############################################################################
#check if the user exit
check_user(){
#	cat /etc/passwd |awk -F: '{print $1}' > /lianxi/note.txt
#	user_number=`wc -l /lianxi/note.txt`
#	for i in `seq $user_number`
#	user_name=`cat /etc/passwd |awk -F: '{print $1}'`
   n1=`cat /etc/passwd|awk -F: '{print $1}'`	
   for i in  $n1
   do
   flag=0
   if [ $name == $i ];
    then
      echo -e "\033[33m"    "*****the user '$i' exists *****\033[0m"
      flag=1
    break ;
   fi
   done 
    if [ $flag -eq 0 ];
       then	
    echo -e "\033[31m the user '$name' isn't exist , please check your character!\033[0m" 
    fi
  
}
#############################################################################
#delete user 
del_user(){
   echo 
   show_name 
   echo
   read -p "^^^^^^please input the user name that you want to delete:" name 
   check_user $name
   if [ $flag -eq 1 ];
    then
    read -p "Notice !!! Are you sure to delete the user ?(y/n)" answer
     if [ $answer == y ];
       then
      /usr/sbin/userdel $name -r; 
             echo -e "\033[31m @^@ Delete user Success! @^@\033[0m"
       else 
       echo -e "\033[31m delay the delete option .\033[0m"
       echo -e "\033[31m  *_* Delete user Failed ! *_* \033[0m"
     fi
     else 
      echo -e "\033[033m  *_*Delete user Failed ! *_* \033[0m"
   fi
   
}
############################################################################
#query user information 
 query_user(){ 
   echo
   show_name
   echo
   read -p "please input the user name you want to query :"  name
   check_user $name 
   if [ $flag -eq 1 ];
     then
      cat /etc/passwd |awk -v var=$name -F: '$1=='var'{print $0}'|awk -F: '{print "name:"$1,"uid:"$3,"gid:"$4,"home directory:"$6}'
      passwd11=`cat /etc/shadow |egrep ^$name|awk -F: '$1="$name"{print $2}'`

    #  if [ "$passwd11"  == "!!" ]; 
      if [ "$passwd11"  == "!!" ]
       then
      echo -e "\033[36m the user is not secure\033[0m "
       else 
      echo -e "\033[36m the user is secure \033[0m"
      fi
     
   fi
}
############################################################################
#show all user names
show_name(){
  alluser=(`cat /etc/passwd |awk -F: '{print $1}'`)
  echo -e "\033[31m**********the all user are :*********\033[0m"
  echo ${alluser[@]}
}
############################################################################
#reset user passwd
reset_user(){
   show_name 
   echo 
   read -p "please input the user name you want to reset password :" name
   check_user $name 
   if [ $flag -eq 1 ];
     then 
          passwd $name 
          echo -e "\033[31m @^@ the password is reset succeed !! @^@\033[0m"
     else
       echo -e  "\033[34m *_* the user is not exist please add the user first !*_*\033[0m"
   fi
}
###########################################################################
#modify user information 
modify_user(){
   show_name
   echo 
   read -p "please input the user name you want to modify:"  name
   check_user $name
  while true 
   do
   echo 
   read -p "which option do you want to modify ? (1.uid/2.gid/3.home_dir)" choose
   case $choose in 
   1)
#     find_uid=`cat /etc/passwd |awk `
     read -p "what uid do you want to modify:" user_id
 #    if [ ]
     usermod -u $user_id -o $name 
     if [ $? == 0 ]
       then
     echo -e "\033[31m @^@ modify success !! @^@\033[0m"
      else 
     echo -e "\033[31m the uid is used,please key again: \033[0m" 
        continue
      fi
    echo -e " \033[31m the modified user information is :\033[0m"
     cat /etc/passwd |awk -v var=$name -F: ' $1=='var'{ print "username:"$1,"uid:"$3,"gid:"$4,"home directory:"$6 }'
     
     ;;
   2)
     read -p "what gid do you want to modity:"  GID
       groupadd -g $GID $name
     if [ $? == 0 ]
       then
          echo -e "\033[31m @^@ modify the gid succeed ! @^@ \033[0m"
     else
#          groupdel $name  
          usermod -g $GID -o $name
          echo -e "\033[31m @^@ modify the gid succeed ! @^@ \033[0m"
     fi
    echo -e " \033[31m the modified user information is :\033[0m"
     cat /etc/passwd |awk -v var=$name -F: ' $1=='var'{ print "username:"$1,"uid:"$3,"gid:"$4,"home directory:"$6 }'
     ;;
   3)
    read -p "what home_directory do you want to modify:"  home_dir
    mkdir $home_dir -p
    usermod -d $home_dir $name
    echo -e " \033[31m the modified user information is :\033[0m"
     cat /etc/passwd |awk -v var=$name -F: ' $1=='var'{ print "username:"$1,"uid:"$3,"gid:"$4,"home directory:"$6 }'
    break;;

   *)break ;;
   esac
done 
}
############################################################################
#show all user information
show_all_user(){
   n1=`cat /etc/passwd |wc -l`
   n2=`cat /etc/passwd |awk -F: '$3<500&&$3>0{print $1}'|wc -l`
   n3=` cat /etc/passwd |awk -F: '$3>499{print $1}'|wc -l`
  echo
 echo -e "\033[43;37m所有的用户信息如下 ：\033[0m"
   cat /etc/passwd |awk -F: '{print "username:"$1,"userid:"$3,"groupid:"$4,"home_directory:"$6,"user_shell:"$7} '|more
 echo
 echo -e "\033[42;37m用户的总的数量为：\033[0m" $n1
 echo 
 echo -e "\033[42;37m系统用户的个数为：\033[0m" $n3
 echo 
 echo -e "\033[42;37m程序用户的个数为：\033[0m" $n2
 echo
}
##############################################################################
main(){
  clear
  while true 
  do
  menu
  read -p"  please choose the option(1-7) :" choice
  case $choice in
  1)
        add_user
	;;
  2)
	del_user
	;;
  3)
	query_user
	;;
  4)
	reset_user
	;;
  5)    
	modify_user
	;;
  6)
	show_all_user
	;;
  7)
	break;;
  *)    break;;
 
  esac
   read -p "         please put any key to continue " key
   clear 
  done
}
main
