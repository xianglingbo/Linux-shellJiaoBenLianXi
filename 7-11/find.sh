#!/bin/bash 

echo -n -e "\e[1;31m please input you need find statistic directory:\e[0m";read u_dir


c_number=`find ${u_dir} -name "*conf" -type f -print |wc -l`
echo -e "\e[32m There is a file number that ends with conf:${c_number}\e[0m"

l_number=`find ${u_dir} -name "*log" -type f -print |wc -l`
echo -e "\e[32m There is a file number that ends with log:${l_number}\e[0m"

d_number=`find ${u_dir} -name "*doc" -type f -print |wc -l`
echo -e "\e[32m There is a file number that ends with doc:${d_number}\e[0m"
