#!/bin/bash
#######################################
#fileme:menu_v2.sh
#function:
#author:xlb
#date:2017-7-7
#version:1.2
#department:devops
#company:sanle learning
########################################


#display user manager menu
menu(){
echo -e "\t\t\e[35m=======================================\e[0m"
echo -e "\t\t\t\e[1;31m***System management tool***\e[0m"
echo -e "\t\t\e[35m=======================================\n\e[0m"
echo -e "\t\t\t\e[1;32;44m1.Display disk space information\e[0m"
echo -e "\t\t\t\e[1;32;44m2.Display network interface information\e[0m"
echo -e "\t\t\t\e[1;32;44m3.Display memory usage information\e[0m"
echo -e "\t\t\t\e[1;32;44m4.Exit menu\e[0m\n"
echo -e "\t\t\e[35m=======================================\n\e[0m"

#Two ways of realizing human-computer interaction

#read -p "please input your option [number 1-4 ]: " opt
echo -n -e "\t\t\e[1;46;34m choose your option [number 1-4 ]:\e[0m";read option
echo 
}

disk(){
	disk_name=`fdisk -l |egrep "^Disk /dev/sd[a-z]" |awk -F: '{print $1}' |awk '{print $2}'`
	disk_num=`fdisk -l |egrep "^Disk /dev/sd[a-z]" |awk -F: '{print $1}' |awk '{print $2}'  |wc -l`
	disk_use=`fdisk -l ${disk_name}`
	echo -e "\e[32mYour computer has ${disk_num} disk.\e[0m"
	echo "${disk_use}"

	
}
network(){
	echo -e "\t\e[1;34mExtract the network card configuration information of this server is as follows:\e[0m"
        INFO=`ifconfig |egrep  "eth?" |head -2`
	echo -e "\e[1;36m${INFO}\e[0m"
}

memory(){
	echo -e "\e[1;42;31mUSER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND\e[0m"
	ps -aux |sort -n -k 4 -r |head -n 11
}

main(){
	while true;
	do 
	menu
	case ${option} in
	1)
		disk
		;;
	2)
		network
		;;
	3)
		memory
		;;
	4)
		exit
		;;
	esac
	done
}	
main;
