#!/bin/bash



#login mysql
echo -n -e "\e[1;35minput you need connect mysql server(username password ipaddress):\e[0m";read u_name u_pwd 
mysql -u${u_name} -p${u_pwd} <<EOF
show databases;
quit
EOF

#create databases
echo -n -e "\e[1;35minput you need create databases:\e[0m";read u_db
mysql -u${u_name} -p${u_pwd} <<EOF
show databases;
create database ${u_db};
show databases; 
quit
EOF

#create tables
echo -n -e "\e[1;35minput you need use databases:\e[0m";read u_db
echo -n -e "\e[1;35minput you need create tables:\e[0m";read t_name
mysql -u${u_name} -p${u_pwd} <<EOF
show databases;
use ${u_db};
create table ${t_name}(id int(5),
name varchar(20),
size varchar(4),
color carchar(10),
price decimal(10));
desc ${t_name}
quit
EOF

#insert data
echo -n -e "\e[1;35minput you need use databases:\e[0m";read u_db
echo -n -e "\e[1;35minput you need use tables:\e[0m";read t_name
echo -n -e "\e[1;35minput you need insert data(id name size color price):\e[0m";read id name size color price
mysql -u${u_name} -p${u_pwd} <<EOF
show databases;
use ${u_db};
insert into ${t_name}(${id},'${name}','${size}','${color}','${price}');
show ${t_name};
quit
EOF


