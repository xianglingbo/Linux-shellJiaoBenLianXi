#!/bin/bash

echo -n -e "\e[35m please input two number:\e[0m";read u_numa u_numb 
echo "${u_numa}+${u_numb}=$((u_numa+u_numb))" 
echo "${u_numa}-${u_numb}=$((u_numa-u_numb))" 
echo "${u_numa}*${u_numb}=$((u_numa*u_numb))" 
div=`echo "scale=2;${u_numa}/${u_numb}" |bc`
if ((u_numa>=u_numb));then
	echo "scale=2;${u_numa}/${u_numb}=${div}" 
else
	echo "scale=2;${u_numa}/${u_numb}=0${div}" 
	
fi
echo "${u_numa}%${u_numb}=$((u_numa%u_numb))" 
echo "${u_numa}**${u_numb}=$((u_numa**u_numb))" 

#echo "scale=2;${u_numa}+${u_numb}" |bc
#echo "scale=2;${u_numa}-${u_numb}" |bc
#echo "scale=2;${u_numa}*${u_numb}" |bc
#echo "scale=2;${u_numa}%${u_numb}" |bc
#echo "scale=2;${u_numa}/${u_numb}" |bc
#echo "scale=2;${u_numa}^${u_numb}" |bc


