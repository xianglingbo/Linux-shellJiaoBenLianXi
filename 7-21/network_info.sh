#!/bin/bash
#finame:network_info.sh

#list ip,dateway,dns,ipname information 
card_num=`lspci |egrep --color "Ethernet" |wc -l`
ip=(`ip add |grep "eth" |grep "inet"|awk '{print $2}'`)
gateway=(`route -n |egrep "^0.0" |awk '{print $2}'`)
dns=(`cat /etc/resolv.conf |grep "^name" |awk '{print $2}'`)
ip_name=(`ip add |egrep -o "[0-9]:.eth[0-9]" |awk '{print $2}'`)

#statistic dateway,dns number
gate_num=` route -n |egrep "^0.0" |awk '{print $2}' |wc -l`
dns_num=`cat /etc/resolv.conf |grep "^name" |wc -l`

#show ip address
for i in `seq 0 $((${card_num}-1))`
do 
	echo -e "\t${ip_name[i]} --------> ${ip[i]}"
done

#show gateway
for j in `seq 0 $((${gate_num}-1))`
do
	echo -e "\tgateway${j} ----> ${gateway}"
done

#show dns
for k in `seq 0 $((${dns_num}-1))`
do
	echo -e "\tdns${k} --------> ${dns[k]}"
done
