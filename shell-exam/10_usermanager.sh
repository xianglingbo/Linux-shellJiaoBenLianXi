#!/bin/bash

#menu list
menu(){
	echo "1.add user"
	echo "2.delete user"
	echo "3.query user information"
	echo "4.reset user's password"
	echo "5.modify user's information"
	echo "6.exit"
}

#1.add user
add(){
	read -p "please input you need add user name:" u_name
	id ${u_name} &>/dev/null
	if [[ $? != 0 ]];then 
		useradd ${u_name} && echo "create ${u_name} success" || echo "create ${u_name} failed"
	else 
		echo "the ${u_name} is already exist"
	fi
}

#2.delete user
delete(){
	read -p "please input you need delete user name:" u_del
	id ${u_del} &>/dev/null
        if [[ $? == 0 ]];then
                useradel -r  ${u_del} && echo "delete ${u_del} success" || echo "delete ${u_del} failed"
        else
                echo "the ${u_del} is already exist"
        fi
}

#3.query user information
query(){
	read -p "please input you need query user name:" u_que
	id ${u_que} 2>/dev/null && cat /etc/passwd |grep ${u_que} || echo "no this user"
}

#4.reset user's password
set_pwd(){
	read -p "please input you set password user name:" re_name
	read -s -p "please input you set password:" re_pwd
	id ${re_name} &>/dev/null 
	if [[ $? == 0 ]];then
		echo "re_pwd" |passwd ${re_name} --stdin 
	else 
		echo "no thid user"
	fi
}

#5.modify user's information
modify(){
	read -p "please input you change user name:" ch_name
	read -p "please input you change user uid:" ch_uid
	read -p "please input you change user gid:" ch_gid
	read -p "please input you change user shell:" ch_shell
	read -p "please input you change user home:" ch_home

	[ -z $ch_uid ] && echo "no change uid" ||usermod -u ${ch_uid} ${ch_name}
	[ -z $ch_gid ] && echo "no change gid" ||usermod -g ${ch_gid} ${ch_name}
	[ -z $ch_uid ] && echo "no change shell" ||usermod -s ${ch_shell} ${ch_name}
	[ -z $ch_uid ] && echo "no change home" ||usermod  -d ${ch_home} ${ch_name}
	
	cat /etc/passwd |grep "$ch_name"
}

#main function 
main(){
	while :
	do
	clear
	menu
	read -p "please input you option:" u_opt
	case  $u_opt in
	1)
	add
	;;
	2)
	delete
	;;
	3)
	query
	;;
	4)
	set_pwd
	;;
	5)
	modify
	;;
	6)
	exit
	;;
	*)
	exit
	;;
	esac
	read
	done
}
main
