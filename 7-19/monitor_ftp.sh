#!/bin/bash
#filename:monitor_ftp.sh
>/var/log/monitor_ftp.log
server_ftp=192.168.0.130
flag=1
#judment the ftp is down or up 
while :
do
	if nc -z 192.168.0.130 21 ;then
		echo "$server_ftp is up"
		if ((flag==1));then
			echo -e "`date +%F_%T`\t$server_ftp is up"  >>/var/log/monitor_ftp.log
			flag=0
		fi
	else
		echo -e "\e[31m warning!!$server_ftp is down\e[0m"
		if ((flag==0));then
			echo -e "`date +%F_%T`\t$server_ftp is down"  >>/var/log/monitor_ftp.log
			flag=1
		fi
	fi
	sleep 1
done
