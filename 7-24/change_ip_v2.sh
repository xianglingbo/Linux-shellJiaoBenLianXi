#!/bin/bash

## menu list ##
menu(){
echo -e "\e[1;32m \t======================================\e[0m"
echo -e "\e[1;34m \t\t1.change ip address\e[0m" 
echo -e "\e[1;34m \t\t2.change netmask\e[0m"
echo -e "\e[1;34m \t\t3.change gateway\e[0m" 
echo -e "\e[1;34m \t\t4.change dns\e[0m"
echo -e "\e[1;34m \t\t5.exit\e[0m"
echo -e "\e[1;32m \t======================================\e[0m"
}

## 1.change ip ##
change_ip(){
echo -e "the type is including (ip,net,gat,dns) option"
read -p "please input you need change ip address (type ip eth?):"u_type u_ip u_eth
jud_ip=`echo "${u_ip}" |egrep "\b([1-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\b" |wc -l`
locate=/etc/sysconfig/network-scripts/ifcfg-${u_eth} 
cat /etc/sysconfig/network-scripts/ifcfg-${u_eth} |grep "^IPADDR="
if [[ $? == 0 ]];then
	if [ -z ${u_ip} ];then
		echo "you can't input emputy"
	elif [[ ${jud_ip} != 1 ]];then
		echo "you input ip address illegal"
	else
		sed -i "/IPADDR/c IPADDR=${u_ip}" ${locate}
		cat ${locate}
	fi
else 
	sed -i "\$a IPADDR=${u_ip}" ${locate}
	cat ${locate}
fi
}

## mian function ##
main(){
	while :
	do
	clear
	menu
	read -p "please input you option:" u_option
	case $u_option in
	1)
	change_ip
	;;
	2)
	change_mask
	;;
	3)
	change_gateway
	;;
	4)
	change_dns
	;;
	5)
	exit
	;;
	*)
	exit
	;;
	esac
	ifdown ${u_eth};ifup ${u_eth}
	read 
	done
}
main
