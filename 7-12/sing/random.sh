#!/bin/sh

#打开文件song.txt 并且存入数组file
file=($(cat song.txt))

#统计歌手数量
num=${#file[@]}

#设立flag作为循环继续的标志
flag='k'

while [ $flag == 'k' ]
do

#如果歌曲全部循环过一次 退出该脚本
if ((num==0))
then
    echo "歌手已经循环完毕"
    break
else
#获取一个随机数 1-歌曲数目
rnd=$(($RANDOM%$num))
echo "随机的歌手是:${file[rnd]}"
#将取过的歌曲从数组中去除
unset file[$rnd]

ww=$(($num-1))
i=$rnd
#去除数组后,重新将数组排列,防止空数组
while [ $i -lt $ww ]
do
     file[$i]=${file[$(($i+1))]}
     ((i++))
done
num=$(($num-1))

echo "继续运行请按k，按任意键退出"
read -n 1  flag
fi
done
