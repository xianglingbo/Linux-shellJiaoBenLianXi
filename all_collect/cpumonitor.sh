#!/bin/bash  
#提取本服务器的IP地址信息  
IP=`ifconfig eth0 | grep "inet addr" | cut -f 2 -d ":" | cut -f 1 -d " "`  
#提取本服务器CPU数量  
cpu_num=`grep -c 'model name' /proc/cpuinfo`  
count_uptime=`uptime |wc -w`  
#当前系统15分钟的平均负载值  
load_15=`uptime | awk '{print $'$count_uptime'}'`  
#获取当前系统单个核心15分钟的平均负载值，结果小于1.0时前面个位数补0  
average_load=`echo "scale=2;a=$load_15/$cpu_num;if(length(a)==scale(a)) print 0;print a" | bc`  
#取上面平均负载值的个位整数  
average_int=`echo $average_load | cut -f 1 -d "."`  
#获取执行shell输入的警告值(0-100以内)  
warn_input=$1  
if [[ ! $warn_input =~ ^[ 0-100 ]+$ ]]  
then  
    exit 0  
else  
    if [[ "$warn_input" -lt 0 || "$warn_input" -gt 100 ]]  
    then  
        exit 0  
    else  
        load_warn=$(($warn_input/100))  
    fi  
fi  
if [ $average_int -gt 0  ]  
then  
    echo "$IP服务器单个核心15分钟的平均负载为$average_load，超过警戒值1.0，请立即处理！！！$(date +'%Y-%m-%d %H:%M:%S')"  
else  
    load_now=`expr $average_load \> $load_warn`  
    if [ $load_now -eq 1 ]  
    then  
        echo "$IP服务器单个核心15分钟的平均负载为$average_load，超过警戒值0.7，请立即处理！！！$(date +'%Y-%m-%d %H:%M:%S')"  
    else  
        echo "$IP服务器单个核心15分钟的平均负载值为$average_load,cpu核心数为$cpu_num,系统15分钟的平均负载为$load_15 负载正常 $(date +'%Y-%m-%d %H:%M:%S')"  
    fi  
fi 
