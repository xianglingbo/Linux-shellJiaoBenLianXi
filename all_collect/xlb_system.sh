#!/bin/bash
#filename:xlb_system.sh



#Extract the IP address information of this host
#ifconfig eth0 | egrep "inet addr" |awk -F: '{print $2}' |awk '{print $1}'


	NUM=`ifconfig |awk '{print $1}' |egrep "^eth?" |wc -l`
echo -e "\t\e[1;34mThe number of network CARDS extracted from this server is: \e[1;35m${NUM}\e[0m"
echo -e "\t\e[1;34mExtract the network card configuration information of this server is as follows:\e[0m"
	INFO=`ifconfig |egrep  "eth?" |head -2`
echo -e "\e[1;36m${INFO}\e[0m"
	CNUM=`grep -c 'model name' /proc/cpuinfo`
echo -e "\t\e[1;34mExtract the CPU number of this server:\e[1;35m${CNUM}\e[0m"

	count_uptime=`uptime |wc -w`
   	load_15=`uptime | awk '{print $'$count_uptime'}'`
echo -e "\t\e[1;34mThe current system has an average load of 15 minutes:\e[1;35m${load_15}\e[0m"


ps aux |sort -k 3 -r -n |head -11

