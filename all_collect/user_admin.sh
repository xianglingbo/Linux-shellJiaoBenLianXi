#!/bin/bash
#filename:user_admin.sh
#function:manage user information (include create,delete,modify,reset password,select users information)
#author:xlb
#company:sanle learing
#date:2017-7-3
#mail:1291015201@qq.com
#version:1.0


#menu
menu(){
	#clear
	echo -e "\t\e[31mthe system users manage menu\e[0m"
	echo -e "\t\t\e[32m1.add user\e[0m"
	echo -e "\t\t\e[32m2.del user\e[0m"
	echo -e "\t\t\e[32m3.query user info\e[0m"
	echo -e "\t\t\e[32m4.change user info\e[0m"
	echo -e "\t\t\e[32m5.reset user password\e[0m"
	echo -e "\t\t\e[32m6.modify user info\e[0m"
	echo -e "\t\t\e[32m7.show all user info\e[0m"
	echo -e "\t\t\e[32m8.exit\e[0m"
	read -p "please input your choose:" u_option
}

#1.add user
add_user(){
	read -p "please input user's name:" u_name
	read -s -p "please input user's password:" u_pwd
	useradd $u_name &>/dev/null
	if (($?=0));then
		echo "$u_pwd" |passwd $u_name --stdin &>/dev/null
		echo -e "\e[32mcongratulation! add user success\e[0m"
	else
		echo -e "\e[31m$u_name already exits"
	fi
}
#2.del user
del_user()
{
	read -p "please input user name:" name
	read -p "please input user number:" num
	for ((i=1;i<=$num;i++))
	do      
        id ${name}$i &>/dev/null
        if [ $? -eq 0 ];then
		userdel -r ${name}$i &>/dev/null
          	echo "the ${name}$i is delete"
        else
          	echo "the ${name}$i does not exist"
        fi
done
}

#3.query user
query_user_info()
{
	read -p "please input user name:" name
	read -p "please input user number:" num
	for i in ` seq $num `
	do
   	id ${name}$i #| awk '{print $1,$2}' 
   	if [ $? -eq 0 ];then
     		cat /etc/passwd | grep ${name}$i |awk -F: '{print $6}'
     		Passwd=`cat /etc/shadow | grep ${name}$i |awk -F: '{print $2}'`
         	if [ "$Passwd" == "!!" ];then
                	echo "${name}${i}没有设置密码"
         	else
                echo "${name}${i}已经设置密码"
         	fi
   	else
     		echo "the user does not exist"
   	fi
	done
}
#4.change user
change_user(){
	read -p "please input your username:" u_name
	useradd $u_name &>/dev/null
	if ! id $u_name &>/dev/null;then
		echo -e "\e[31m${u_name} already exits\e[0m"
		change_user
	fi
	
	while :
	do 
	echo -e "\e[31m************************************\e[0m"
	echo -e "\e[32m1.change uid\e[0m" 
	echo -e "\e[32m2.change gid\e[0m" 
	echo -e "\e[32m3.change annotation\e[0m" 
	echo -e "\e[32m4.change home directory\e[0m" 
	echo -e "\e[32m5.change user shell\e[0m" 
	echo -e "\e[32m6.change username\e[0m" 
	echo -e "\e[32m7.exit\e[0m" 
	echo -e "\e[31m************************************\e[0m"
	read -p "please input your choose:" key
	case $key in
	1)
		read -p "please input your uid:" uid
		read -p "are you sure y/n" jud
		if [ "$jud"="y" ];then
			usermod -u $uid $u_name
			cat /etc/passwd |egrep "^$u_name"
		else
			echo -e "\e[31mgive up this opration\e[0m"
		fi
		;;
	2)
		read -p "please input your gid:" gid
		read -p "are your sure y/n:" jud
		if [ "$jud"="y" ];then
			groupadd -g $gid a$gid &>/dev/null
			usermod -g $gid $u_name
			cat /etc/passwd |egrep $u_name
		else 
			echo -e "\e[31mgive up this opration\e[0m"
		fi
		;;
	3)
		read -p "please input your argument:" arg
		read -p "are  your sure y/n:" judge
		if [ "$judge"="y" ];then
			usermod -c $arg $u_name
			cat /etc/passwd |egrep $_name
		else
			echo -e "\e[31mgive up this opration\e[0m"
		fi
		;;
	4)
		read -p "please input your directory:" arg
		read -p "are you sure y/n:" judge
		if [ "$judge"=="y" ];then
			usermod -d $arg $u_name
			mv /home/$u_name $arg
			cat /etc/passwd |egrep $u_name
		else 
			echo -e "\e[31mgive up this opration"
		fi
		;;
	5)
		read -p "please input you shell:" arg
		read -p "are you sure y/n:" judge
		if [ "$judge"="y" ];then
			usermod -s $arg $u_name
			cat /etc/passwd |egrep $u_name
		else 
			echo -e "\e[31mgire up this opration\e[0m"
		fi
		;;
	6)
		read -p "please input your username:" arg
		read -p "are you sure y/n:" judge
		if [ "$judge"="y" ];then
                        usermod -l $arg $u_name
			u_name=$arg
                        cat /etc/passwd |egrep $u_name
                else
                        echo -e "\e[31mgire up this opration\e[0m"
                fi
                ;;
	7)
		break
		;;
	*)
		echo -e "\e[1;35merror argument\e[0m"
		;;
	esac
	done
}
#5.reser user passwd
set_user_pass()
{
   	cat /etc/shadow |awk '/!!/'| awk -F: '{print $1}'
   	read -p "请选择要设置密码的用户：" name
   	passwd $name
   	echo "设置${name}密码成功"
}
#6.modify user
modify_user_info()
{
   	cat /etc/passwd | awk -F: '{print $1}'
   	read -p "请选择要修改信息的用户：" name
   	read -p "请修改用户的ID号:" uid
   	read -p "请修改组的ID号:" gid
   	usermod -u $uid $name
   	groupmod -g $gid $name
   	Pass=`cat /etc/shadow | grep $name |awk -F: '{print $2}'`
        if [ "$Pass" == "!!" ];then
           	echo "${name}该用户未设置密码，请为他设置密码"
           	passwd $name
        else
           	echo "${name}已设置了密码"
        fi
   	echo "修改后的信息如下所示:"
   	id $name
}
#7.show user
show_user_info()
{  
   	user_all=`cat /etc/passwd | awk -F: '{print $1}' | wc -l`
   	user_sys=`cat /etc/passwd | awk -F: '$3>0 && $3<500{print $1}' | wc -l `   
	user_pro=`cat /etc/passwd | awk -F: '$3>=500 && $3<60000{print $1}'| wc -l`
   	echo -e "\t所有用户的数量为:"$user_all
   	echo -e "\t系统用户的数量为:"$user_sys
   	echo -e "\t程序用户的数量为:"$user_pro
}

#main
main(){
	while :
	do
	menu
	case $u_option in 
	1)
		add_user
		;;
	2)
		del_user
		;;
	3)
		query_user_info
		;;
	4)
		change_user
		;;
	5)
		set_user_pass
		;;
	6)
		modify_user_info
		;;
	7)
		show_user_info
		;;
	8)
		exit
		;;
	*)
		echo -e "\e[0;31m please choose 1-7 number\e[0m"
        	;;
	esac
	done		
}
main;
