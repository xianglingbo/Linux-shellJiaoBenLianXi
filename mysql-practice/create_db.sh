#!/bin/bash

USER="root"
PASS="123123"

mysql -u $USER -p$PASS <<EOF 2> /dev/null
CREATE DATABASE student;
EOF

[ $? -eq 0 ] && echo create db ok || echo the db already exist

mysql -u $USER -p$PASS student <<EOF 2> /dev/null
CREATE TABLE students(
id int,
name varchar(20),
mark int,
dept varchar(6)
);
EOF
[ $? -eq 0 ] && echo create table ok || echo the table already exist
