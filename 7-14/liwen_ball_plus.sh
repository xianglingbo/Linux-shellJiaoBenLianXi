#! /bin/bash

#随机生成目标号码，存入数组result
function create_random_number
{	
	echo "系统正在生成中奖号码！请稍等..."
	sleep 1
	echo -n "红球："
	sleep 1
	unset result
	red=(`seq 33`)
	n=33

	for i in {0..5}
	do	
		r=`echo $(($RANDOM%$n))`
		result[$i]=${red[$r]}
		echo -n "${result[$i]} "
		sleep 1
		((n--))
		unset red[$r]
		red=(`echo ${red[@]}`)
	done
	result[6]=`echo $(($RANDOM%16+1))`
	echo 
	sleep 1
	echo -n "蓝球："
	sleep 1
	echo "${result[6]}"
}

#接受用户输入的号码，存入数组number
function receive_user_number
{
	unset number
	for i in {0..5}
	do	
	    echo -n "请输入第$(($i+1))个红球号码："	
		read number[$i]		
	done
	number1=(`echo ${number[@]}`)
	read -p "请输入1个篮球号码：" number[6]
	read -p "请输入注数：" z
	clear
	echo "OK!Let's begin..."
	echo "your red ball is: ${number1[@]}"
	echo "your blue ball is: ${number[6]}"
	echo "times : $z"
	echo 
}

#生成中奖号码，存入数组a
function create_win_number
{
	unset a
	k=0
	g=0
	for i in {0..5}
	do
		for j in {0..5}
		do
			if [ ${result[$i]} -eq ${number[$j]} ]
			then 
				a[k]=${result[$i]}
				((k++))
			fi			
		done
	done

	if [ ${result[6]} -eq ${number[6]} ]
	then
		a[k]=${result[6]}
	fi
	
	echo
	echo "您中奖的号码为：${a[@]}"
}

function prize
{
	case ${#a[@]} in
	0)
		echo "很遗憾！您未中奖>O<"

		;;
	1)
		echo "恭喜你！获得七等奖！奖金1000*$z=$((1000*$z))RMB!!!"
		
		;;
	2)
		echo "恭喜你！获得六等奖！奖金2000*$z=$((2000*$z))RMB!!!"

		;;
	3)	
		echo "恭喜你！获得五等奖！奖金4000*$z=$((4000*$z))RMB!!!"
		
		;;
	4)
		echo "恭喜你！获得四等奖！奖金10000*$z=$((10000*$z))RMB!!!"
		
		;;
	5)
		echo "恭喜你！获得三等奖！奖金50000*$z=$((50000*$z))RMB!!!"

		;;
	6)
		echo "恭喜你！获得二等奖！奖金200000*$z=$((200000*$z))RMB!!!"
		
		;;
	7)
		echo "恭喜你！获得一等奖！奖金800000*$z=$((800000*$z))RMB!!!"
		
		;;
	esac
	echo
}

function run_game
{
	clear
	echo -e "\t\t\e[5;31m是不是已经饥渴难耐了！快来下一注吧！\e[0m"
	receive_user_number		
	create_random_number
	create_win_number
	prize
	echo "下一个幸运儿可能就是你哦！"
	read -p "还要再来一次吗?(y/n): " ch
	[ "$ch" == "y" ] && run_game || echo "see you..."
}

run_game
