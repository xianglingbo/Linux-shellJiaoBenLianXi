#!/usr/bin/expect

set timeout 3
spawn ssh root@192.168.0.139
expect {
	"(yes/no)?"
	{
	send "yes\n"
	expect "password:"
	send	"123123\n"
	exit 40
	}
	"password:"
	{
	sed "123123\n"
	exit 50
	}
	"#"
	{
	send "date\n"
	exit 60
	}
}
expect eof
