#!/bin/bash
# 随机抽取文件中的歌手唱歌,不重复抽取

ThreeTwoOne(){
	echo -n "我们一起倒数三秒:"
	for j in {3..1} ; do
		echo -n "$j "
		sleep 1
	done
	echo
}

[[ -n $1 ]] && singer_file=$1 || singer_file=singer.txt
singer=($(cat $singer_file))
n=${#singer[*]}

echo "共有${n}位歌手:${singer[*]}"

for i in $(seq $n -1 2) ; do
	ThreeTwoOne
	rand=$[RANDOM % i]
	echo "${singer[$rand]}唱歌"

	let i=i-1
	if (( $i != $rand )) ; then
		# 交换singer[i-1]和singer[rand],把singer[rand]放到最后面
		t=${singer[$i]}
		singer[$i]=${singer[$rand]}
		singer[$rand]=$t
	fi
done
ThreeTwoOne
echo "${singer[0]}唱歌"
