#!/bin/bash

#date_now=`date +%Y-%m-%d\ %H:%M:%S`
time1=$(($(date +%Y-%m-%d\ %H:%M:%S) - $(date +%s -d '1970-01-01 00:00:00')))
echo $time1


time2=$(($(date +%s -d '2017-07-12 17:30:00') - $(date +%s -d '1970-01-01 00:00:00')))
echo $time2

time3=`echo "${time2}-${time1}" |bc`
echo $time3

echo "${time3}/60" |bc
echo "${time3}/3600" |bc
