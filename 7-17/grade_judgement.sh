#!/bin/bash

read -p "please input your grade :" u_grade
if [[ -z $u_grade ]];then
	echo "please agin:" 
	exit
fi
if echo $u_grade |grep -v "[0-9]";then
	echo "please input the range of 0-100 nunber!"
	exit
fi
if (( $u_grade >= 90 && $u_grade <= 100 ));then
	echo "A"
elif (($u_grade >= 80 && $u_grade <= 89 ));then
	echo "B"
elif (($u_grade >= 70 && $u_grade <= 79 ));then
	echo "C"
elif (($u_grade >= 60 && $u_grade <= 69 ));then
	echo "D"
elif (($u_grade >= 0 && $u_grade <= 59 ));then
	echo "E"
else
	echo "please input the 0-100 nunber!"
fi
