#!/bin/bash
aa=`cat xferlog |awk '{print $7}' |sort |uniq`
#aa=`cat xferlog |cut -d " " -f7,8|sort|awk '{print $1}'|sort|uniq`
for i in `echo $aa`
do 
a=`cat xferlog |egrep "\b$i\b"|awk '{print $8}'|paste -s -d"+"|bc`
size=`echo "scale=2;$a/1000/1000"|bc`
if echo $size |grep "^[0-9]" &>/dev/null
then
	echo "$i ${size}M" 
else
	echo "$i 0${size}M"
fi
done
