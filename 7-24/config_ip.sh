#!/bin/bash
#function:config ip 

#receive user's ip information
re_info(){
	read -p "please input ip address:" u_ip
	read -p "please input netmask:" u_mask
	read -p "please input gateway:" u_gateway
	read -p "please input dns:" u_dns
	all_inffo=($u_ip $u_mask $u_gateway $u_dns)
}

#modify ip information
modify_ip(){
	ip_file=/etc/sysconfig/network-scripts/ifcfg-eth0
p
	cp $ip_file /backupu/ifcfg-eth0
	sed -i.ipbak "/IPADDR/c IPADDR=$u_ip" $ip_file
	sed -i.netmaskbak "/NETMASK/c NETMASK=$u_mask" $ip_file
	sed -i.gatewaybak "/GATEWAY/c GATEWAY=$u_gateway" $ip_file
	sed -i.dnsbak "/DNS/c DNS1=$u_dns" $ip_file
}

#check ip
check_ip(){
	re_info
	for i in ${!all_info[@]}
	do
	if (( i != 1 ));then
		if echo ${all_info[i]} |egrep "\b([1-9]|[1-9][0-9]|1[0-9]{2}|2[01][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\b"
		then
			modify_ip
	
		else
			re_info
		fi
	else
                if echo $u_mask |egrep "\b(255)(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\b"
                then
                        modify_ip
        
                else
                        re_info
                fi
	fi 
	done
}
check_ip
ifdown eth0;ifup eth0
