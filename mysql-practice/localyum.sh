#!/bin/bash
#filename:localyum.sh
#author:xlb
#date:2017-7-4


#see if the yum directory exits
dir="/yum"
if [ -e $dir ];then
	echo -e "\e[1;31m${dir} directory already exits\e[0m"
else 
	mkdir /yum &>/dev/null
	echo -e "\e[1;32m${dir} directory already created\e[0m"
fi
#see if the mount is successfully
mount /dev/cdrom /yum &>/dev/null
if [ $? -eq 0 ];then
	echo -e "\e[1;32mThe CD has been mounted successfully\e[0m"
else
	locate=`df -Th |tail -1 | awk '{print $7}'` &>/dev/null
	if [ "$locate"=="/yum" ];then
		echo -e "\e[1;32mThe CD has been to mount\e[0m"
	else
		echo -e "\e[1;31mThe CD has been to mount failed\e[0m"
	fi
fi
#change directory
cd /etc/yum.repos.d
file="/etc/yum.repos.d/localyum.repo"
if [ -e $file ];then 
	echo -e "\e[1;31m${file} file is exits\e[0m"
else 
	touch localyum.repo &>/dev/null
	cat >localyum.repo <<EOF
[local_yum]
name=localyum
baseurl=file:///yum
enabled=1
gpgcheck=0
EOF
	if [ $? -eq 0 ];then
		echo -e "\e[1;32m${file} script write success"
	else
		echo -e "\e[1;31m${fiel} script write failed"
	fi
fi
#move others file 
of="/etc/yum.repos.d/bat"
if [ -e $of ];then
	echo -e "\e[1;31m${of} directory already exits that not need to be created\e[0m"
else
	mkdir $of &>/dev/null
	echo -e "\e[1;32m${of} directory create successfully\e[0m"
	mv /etc/yum.repos.d/Cent* $of &>/dev/null
fi
#test yum
yum clean all &>/dev/null
if [ $? -eq 0 ];then
	echo -e "\e[1;32mclean yum cache success\e[0m"
else 
	echo -e "\e[1;31mclean yum cache failed\e[0m"
fi
yum list &>/dev/null
if [ $? -eq 0 ];then
	echo -e "\e[1;32myou can use localyum\e[0m"
else 
	echo -e "\e[1;31mthe localyum has error and cannot be used\e[0m"
fi
