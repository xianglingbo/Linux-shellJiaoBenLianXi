#!/bin/bash

USER="root"
PASS="123123"

if [ $# -ne 1 ];then
	echo $0 datafile
	echo 
fi

data=$1

while read line;
do
	oldIFS=$IFS
	IFS=,
	
	values=($line)
	values[1]="\"`echo ${values[1]} |tr ' ' '#'`\""
	values[3]="\"`echo ${values[3]}`\""

	write=`echo ${values[@]} |tr ' #' ', ' `
	IFS=$oldIFS
	mysql -u $USER -p$PASS student <<EOF
insert into students values($write)
EOF
done< $data

[ $? -eq 0 ] && echo write data ok || echo write data failed
