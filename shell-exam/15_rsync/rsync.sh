#!/usr/bin/expect

[ -e /etc/rsyncd.conf ] && echo "the rsync config file write" || cat >/etc/rsyncd.conf <<EOF
port=873
address=0.0.0.0
uid=nobody
gid=nobody
read only=no

[share]
path=/tmp/test
EOF

chkconfig rsync on
netstat -ntpl | grep 873
service xinetd restart
rsync --list-only rsync://192.168.11.58/share/

ti=`date`
[ -f /var/log/rsync.log ] || touch /var/log/rsync.log
ssh root@192.168.11.189 mkdir /backup_58_var_log/
rsync -av --delete /var/log/ root@192.168.11.180:/backup_58_var_log/
[ $? -eq 0 ] && echo "${ti} backup success" >>/var/log/rsync.log || echo "${ti} backup failed"

