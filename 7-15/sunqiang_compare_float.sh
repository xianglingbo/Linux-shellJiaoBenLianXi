#!/bin/bash

# 先判断num1是否大于num2
#	如果num1不大于num2
#	要么num1==num2
#	要么num1<num2
# 	使用>=来判断是否相等

read -p "please input two float numbers:"num1 num2
if (($(echo "$num1 > $num2" |bc)==0))
then
    if (($(echo "$num1>=$num2" |bc)==1))
    then
        echo "$num1 = $num2"
    else
        echo "$num1 < $num2"
    fi
else
    echo "$num1 > $num2"
fi
