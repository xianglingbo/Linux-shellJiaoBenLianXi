#!/bin/bash
size=(`find $1 -type f -size +10M 2>/dev/null -exec ls -l {} \; |awk '{print $5}'`)
q=0
for i in "${size[@]}"
do
	(( q=q+i ))
done
((size1=q/1024/1024))
number=`find $1 -type f -size +10M 2>/dev/null -exec ls -l {} \;|wc -l`
echo "超过10M的文件数目：$number"
echo "这些文件的大小是：$size1 M"
