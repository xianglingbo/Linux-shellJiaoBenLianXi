#!/bin/bash
numbers=`find $1 -type f  -size +10M 2>/dev/null |xargs ls -l 2>/dev/null|wc -l`
size=`find $1 -type f  -size +10M 2>/dev/null |xargs ls -l|awk '{print $5}'|tr -s "\n" "+"`
echo "超过10M的文件数目：$numbers"
size1=`echo 0${size}0|bc`
size2=`echo "$size1/1024/1024"|bc` 
echo  "这些文件的总大小是：$size2 M" 
