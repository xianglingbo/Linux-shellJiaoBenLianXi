#!/bin/bash
#filename:group_user.sh
#author:xlb

echo -e "\e[1;31minput the groupname you need to query:\e[0m" 
read groupname
#read -p "input the groupname you need to query:" groupname
groupid=`cat /etc/group |egrep ${groupname} |awk -F: '{print $3}'`
if [ $? -eq 0 ];then
	echo -e "\e[1;34mThe ${groupname} groupid is ${groupid} and has the following users\e[0m"
	username=`cat /etc/passwd |egrep ${groupid} |awk -F: '{print $1}'`
	line=`cat /etc/passwd |egrep ${groupid} |awk '{print $1}' |wc -l`
	if [ "$line" -ge 0 ];then
		echo -e "\e[0;35m${username}\e[0m"
	else 
		echo -e "\e[031m${groupid} has no users\e[0m"
	fi
else 
	echo -e "\e[1;31mThe ${groupname} doesn't exits\e[0m"
fi

