#!/bin/bash
#Author:向灵波
#Date:2017/05/18
#Mail:1291015201@qq.com
#Phone:15773138001
#Theme:系统用户管理

script()
{
   echo -e "\t========================================================="
   echo -e "\t\t\e[1;34;40m System user management script \e[0m"
   echo -e "\t=========================================================\n"
   echo -e "\t\t\e[1;31m 1.Addition user \e[0m"
   echo -e "\t\t\e[1;32m 2.Delete user \e[0m"
   echo -e "\t\t\e[1;33m 3.Query user information \e[0m"
   echo -e "\t\t\e[1;34m 4.Set user password \e[0m"
   echo -e "\t\t\e[1;35m 5.Modify user information \e[0m"
   echo -e "\t\t\e[1;36m 6.Show user information \e[0m"
   echo -e "\t\t\e[1;31m 7.Exit the script\n \e[0m"
   echo -e "\t=========================================================\n"
}
#1#
Addition_user()
{
read -p "please input user name:" name
read -p "please input user number:" num
for i in ` seq $num `
do
        id ${name}$i &>/dev/null
        if [ $? -ne 0 ]
        then
          useradd ${name}$i &>/dev/null
	  echo "add ${name}$i succeed"
        else
          echo "this ${name}$i already exist"
        fi
done
}
#2#
Delete_user()
{
read -p "please input user name:" name
read -p "please input user number:" num
for ((i=1;i<=$num;i++))
do
        id ${name}$i &>/dev/null
        if [ $? -eq 0 ]
        then
          userdel -r ${name}$i &>/dev/null
          echo "the ${name}$i is delete"
        else
          echo "the ${name}$i does not exist"
        fi
done
}
#3#
Query_user_info()
{
read -p "please input user name:" name
read -p "please input user number:" num
for i in ` seq $num `
do
   id ${name}$i #| awk '{print $1,$2}' 
   if [ $? -eq 0 ]
   then
     cat /etc/passwd | grep ${name}$i |awk -F: '{print $6}'
     Passwd=`cat /etc/shadow | grep ${name}$i |awk -F: '{print $2}'`
   	 if [ "$Passwd" == "!!" ]
	 then
        	echo "${name}${i}没有设置密码"
   	 else 
       		echo "${name}${i}已经设置密码"
	 fi
   else 
     echo "the user does not exist"
   fi
done 
}
#4#
Set_user_pass()
{
   cat /etc/shadow |awk '/!!/'| awk -F: '{print $1}'
   read -p "请选择要设置密码的用户：" name
   passwd $name
   echo "设置${name}密码成功"
}
#5#
Modify_user_info()
{
   cat /etc/passwd | awk -F: '{print $1}'
   read -p "请选择要修改信息的用户：" name
   read -p "请修改用户的ID号:" uid
   read -p "请修改组的ID号:" gid
   usermod -u $uid $name
   groupmod -g $gid $name
   Pass=`cat /etc/shadow | grep $name |awk -F: '{print $2}'`
       	if [ "$Pass" == "!!" ]
	then
	   echo "${name}该用户未设置密码，请为他设置密码"
           passwd $name    
   	else
           echo "${name}已设置了密码"
       	fi
   echo "修改后的信息如下所示:"
   id $name
}
#6#
Show_user_info()
{
   user_all=`cat /etc/passwd | awk -F: '{print $1}' | wc -l`
   user_sys=`cat /etc/passwd | awk -F: '$3<500{print $1}'| wc -l `
   user_pro=`cat /etc/passwd | awk -F: '$3>=500 && $3<60000{print $1}'| wc -l`
   echo -e "\t所有用户的数量为:"$user_all
   echo -e "\t系统用户的数量为:"$user_sys
   echo -e "\t程序用户的数量为:"$user_pro
}

main()
{
   while true 
   do
   script
   read -p "Enter your option (first line number):" option
   case $option in
   1)
       Addition_user
       ;;
   2)
       Delete_user
       ;;
   3)
       Query_user_info
       ;;
   4)
       Set_user_pass
       ;;
   5)
       Modify_user_info
       ;;
   6)
       Show_user_info
       ;;
   7)
       break
       ;;
   *)
   	echo -e "\e[0;31m 请选择1-7内的数！\e[0m"
	;;
   esac     
   done   
}
main;
