#!/bin/bash 

used=`free -m |grep "^Mem" |awk '{print $3}'`
total=`free -m |grep "^Mem" |awk '{print $2}'`
us=$(echo "scale=2;${used}/$total" |bc |cut -d"." -f2)
[ $us -lt 80 ] && echo -e "\e[32m memory enough\e[0m" ||echo -e "\e[31m memory not enough\e[0m"

