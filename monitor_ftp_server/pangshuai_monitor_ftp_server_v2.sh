#!/bin/bash
#discribe:This is a monitoring system which used to monitor FTP service.
#author  :pangshuai
#date    :2016-5-13
#company :Chinaitsoft Daming Lake
###################################################################################
#menu system
menu(){
echo -e "\033[31m\t\t************ FTP服务监控系统************\033[0m"
echo -e "\n"
echo -e "\033[32m\t\t************1    运行监控   ************\033[0m"
echo -e "\n"
echo -e "\033[33m\t\t************2  服务关闭统计 ************\033[0m"
echo -e "\n"
echo -e "\033[34m\t\t************3    终止监控   ************\033[0m"
echo -e "\n"
echo -e "\033[35m\t\t************4    退出系统   ************\033[0m"
}
####################################################################################
#monitor running system
>normal.log
monitor(){
j=0
while true
do
	dat=`date`
	nc -z  172.16.10.31 21 &>/dev/null  
	if (($? == 0))
	then 
		echo "$dat the 172.16.10.31 ftp service running" >>normal.log
		(($j != 0))&&echo "the ftp service had down $j seconds" >>error.log
		j=0
	else
		(($j == 0))&&echo "$dat  the 172.16.10.31 ftp service down" >>error.log
		((j++))
	fi
	sleep 1
	num=`cat normal.log|wc -c`
	(($num == 10000))&&  >normal.log
done
}
####################################################################################
#statistic the down times
statistics(){
clear
d_num=`cat error.log|grep CST|wc -l`
echo "the FTP service had down $d_num times"
cat error.log
}
####################################################################################
#shutdown the monitoring system
shutdown(){
process_num=`ps aux|grep "bash ftp_tance.sh"|tr -s ' '|cut -d ' ' -f2|head -n 1`
kill -9 $process_num&>/dev/null&&echo "the monitoring system has been shutdown"
}
####################################################################################
#main progrem
main(){
clear
while true
do
menu	
read -p "please choose the option(1-4):" choice
case $choice in
1)
	(monitor)&
	;;
2)
	statistics
	;;
3)
	shutdown
	;;
4)
	break;;
*)	
	break;;
esac
read -p "please put any key to continue" key
clear
done
}
main
