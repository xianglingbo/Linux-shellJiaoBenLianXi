#!/bin/bash

#########################################
#filename:first.sh
#function:say hello,make some directory
#author:xlb
#date:2017-7-6
#version:1.0
#department:devops
#company:sanle learning
########################################


#display user manager menu
echo -e "\t\t\e[35m=======================================\e[0m"
echo -e "\t\t\t\e[1;31muser manager menu\e[0m"
echo -e "\t\t\e[35m=======================================\n\e[0m"
echo -e "\t\t\t\e[1;32;44m1.add user\e[0m"
echo -e "\t\t\t\e[1;32;44m2.del user\e[0m"
echo -e "\t\t\t\e[1;32;44m3.modify user\e[0m"
echo -e "\t\t\t\e[1;32;44m4.exit\e[0m\n"
echo -e "\t\t\e[35m=======================================\e[0m"
