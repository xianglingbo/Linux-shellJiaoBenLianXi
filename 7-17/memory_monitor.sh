#!/bin/bash

free=`free -m |egrep "^Mem" |awk '{print $3}'`
total=`free -m |egrep "^Mem" |awk '{print $2}'`
us=$(echo "scale=2;${free}/$total" |bc |cut -d"." -f2)
[ $us -lt 5 ] && echo -e "\e[31m Memory free space ratio is ${us}%;memory not enough\e[0m" ||echo -e "\e[32m Memory free space ratio is ${us}%;memory enough\e[0m"

