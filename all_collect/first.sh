#!/bin/bash

#########################################
#filename:first.sh
#function:say hello,make some directory
#author:xlb
#date:2017-7-6
#version:1.0
#department:devops
#company:sanle learning
########################################


#say hello
echo -e "\e[1;32mhello,world!!\e[0m"
cd /root/lianxi/

#create directory
mkdir $RANDOM-sanle.txt
ls /root/lianxi
echo -e "\e[1;34m########THE END#########\e[0m"
