#!/bin/bash

## menu list ##
menu(){
echo -e "\e[1;32m \t======================================\e[0m"
echo -e "\e[1;34m \t\t1.change ip address\e[0m" 
echo -e "\e[1;34m \t\t2.change netmask\e[0m"
echo -e "\e[1;34m \t\t3.change gateway\e[0m" 
echo -e "\e[1;34m \t\t4.change dns\e[0m"
echo -e "\e[1;34m \t\t5.exit\e[0m"
echo -e "\e[1;32m \t======================================\e[0m"
}

## 1.change ip ##
change_ip(){
read -p "please input you need change ip address (ip eth?):" u_ip u_eth
jud_ip=`echo "${u_ip}" |egrep "\b([1-9]|[1-9][0-9]|1[0-9]{2}|2[01][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\b" |wc -l`
locate=/etc/sysconfig/network-scripts/ifcfg-${u_eth} 
cat /etc/sysconfig/network-scripts/ifcfg-${u_eth} |grep "^IPADDR="
if [[ $? == 0 ]];then
	if [ -z ${u_ip} ];then
		echo "you can't input emputy"
	elif [[ ${jud_ip} != 1 ]];then
		echo "you input ip address illegal"
	else
		sed -i "/IPADDR/c IPADDR=${u_ip}" ${locate}
		cat ${locate}
	fi
else 
	sed -i "\$a IPADDR=${u_ip}" ${locate}
	cat ${locate}
fi
}

## 2.change netmask ##
change_netmask(){
read -p "please input you need change netmask (netmask eth?):" u_mask u_eth
jud_mask=`echo "${u_mask}" |egrep "\b([1-9]|[1-9][0-9]|1[0-9]{2}|2[01][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\b" |wc -l`
locate=/etc/sysconfig/network-scripts/ifcfg-${u_eth} 
cat /etc/sysconfig/network-scripts/ifcfg-${u_eth} |grep "^NETMASK="
if [[ $? == 0 ]];then
	if [ -z $u_mask ];then
		echo "you can't input emputy"
	elif [[ ${jud_mask} != 1 ]];then
		echo "you input ip address illegal"
	else
		sed -i "/NETMASK/c NETMASK=${u_mask}" ${locate}
		cat ${locate}
	fi
else 
	sed -i "\$a NETMASK=${u_mask}" ${locate}
	cat ${locate}
fi
}

## 3.change gateway ##
change_gateway(){
read -p "please input you need change dateway (gateway eth?):" u_gateway u_eth
jud_gateway=`echo "${u_gateway}" |egrep "\b([1-9]|[1-9][0-9]|1[0-9]{2}|2[01][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\b" |wc -l`
locate=/etc/sysconfig/network-scripts/ifcfg-${u_eth} 
cat /etc/sysconfig/network-scripts/ifcfg-${u_eth} |grep "^GATEWAY="
if [[ $? == 0 ]];then
	if [ -z ${u_gateway} ];then
		echo "you can't input emputy"
	elif [[ ${jud_gateway} != 1 ]];then
		echo "you input ip address illegal"
	else
		sed -i "/GATEWAY/c GATEWAY=${u_gateway}" ${locate}
		cat ${locate}
	fi
else 
	sed -i "\$a GATEWAY=${u_gateway}" ${locate}
	cat ${locate}
fi
}

## 4.change dns ##
change_dns(){
read -p "please input you need change dns (dns eth?):" u_dns u_eth
jud_dns=`echo "${u_dns}" |egrep "\b([1-9]|[1-9][0-9]|1[0-9]{2}|2[01][0-9]|22[0-3])(\.([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])){3}\b" |wc -l`
locate=/etc/sysconfig/network-scripts/ifcfg-${u_eth} 
cat /etc/sysconfig/network-scripts/ifcfg-${u_eth} |grep "^DNS"
if [[ $? == 0 ]];then
	if [ -z ${u_dns} ];then
		echo "you can't input emputy"
	elif [[ ${jud_dns} != 1 ]];then
		echo "you input ip address illegal"
	else
		sed -i "/DNS/c DNS1=${u_dns}" ${locate}
		cat ${locate}
	fi
else 
	sed -i "\$a DNS1=${u_dns}" ${locate}
	cat ${locate}
fi
}

## mian function ##
main(){
	while :
	do
	clear
	menu
	read -p "please input you option:" u_option
	case $u_option in
	1)
	change_ip
	;;
	2)
	change_mask
	;;
	3)
	change_gateway
	;;
	4)
	change_dns
	;;
	5)
	exit
	;;
	*)
	exit
	;;
	esac
	ifdown ${u_eth};ifup ${u_eth}
	read 
	done
}
main
