#!/bin/bash

for i in {1..254}
do
	ping -c 2 -w 1 -i 0.2 192.168.0.$i &>/dev/null
	if (( $? == 0 ));then
		echo -e "\t 192.168.0.$i" >>up.txt
		up_num=`cat up.txt |wc -l`
		echo "the pc up total $up_num and list:"
		up_list=`cat up.txt`
		echo "$up_list"
		
	else
		echo -e "\t 192.168.0.$i" >>down.txt
		down_num=`cat down.txt |wc -l`
		echo "the pc up total $down_num and list:"
		down_list=`cat down.txt`
		echo "$down_list"
	fi
done
