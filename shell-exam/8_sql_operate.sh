#!/bin/bash

#create databases
mysql -uroot -p123 <<EOF
create database sanle;
show databases;
EOF
[ $? -eq 0 ] && echo "create db sanle ok" || echo "create db sanle failed"

#create table
mysql -uroot -p123  <<EOF
use sanle;
create table student(
id int primary key,
name varchar(20),
sex varchar(4),
address varchar(20),
phone int,
grade int
);
EOF
[ $? -eq 0 ] && echo "create table ok" || echo "create table failed"

#insert data and show all datas 
mysql -uroot -p123 <<EOF
use sanle;
insert into student values(1,"ying","man","changsha",121341,78);
insert into student values(2,"zhang","man","yueyang",121341,68);
insert into student values(3,"xiang","man","jishou",121341,68);
insert into student values(4,"hu","man","yiyang",121341,80);
select * from student;
EOF


