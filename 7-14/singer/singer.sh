#!/bin/bash

#menu list
menu(){
	echo "1.Display all singer and songs."
	echo "2.Choice songs"
	echo "3.Add singer"
	echo "4.Add songs"
	echo "5.Exit"
	singer=(`cat singer.txt`)
	songs=(`cat songs.txt`)
	num=`cat singer.txt|wc -w`
	song_num=`cat songs.txt|wc -w`
	read -p "please input your option:" u_opt
}
#1.display all singer and songs
display_singer_somgs(){
  	echo -e "\e[35m the singer lists as follows:\e[34m${singer[@]}\e[0m,total is: \e[32m${num}\033[0m"
        echo -e "\e[35m the songs we can chosed as follows:\e[33m${songs[@]}\e[0m,total is: \e[32m${song_num}\e[0m"       
}

#choice songs
choice_song(){

#(teacher feng ways)create an array to store singers random one singer to sing for us
#while :
#do 
#	lucky_num=$((RANDOM%${#singer[@]}))	
#	echo -e "please \e[31m ${singer[lucky_num]} \e[0m sing a song for us :"
#	read -p "please welcome next singer"
#	unset singer[lucky_num]
#	singer=(${singer[@]})
#	echo "there are ${singer[@]} nerver sing for us, please wait a moment"
#	echo "${singer[@]}"
#	if (( ${#singer[@]} == 0 ));then
#		echo "all singers have sing over,please run again"
#		exit
#	fi	
#done

#(my ways)
        singer_num=${#singer[@]}
        lucky_singer=$((RANDOM%singer_num))
        song_num=${#songs[@]}
        lucky_song=$((RANDOM%song_num))
        echo -e "\e[1;35m The chosen singer and the song to sing:\e[0m"
        for i in {3..1}
        do
                echo -e "\t\e[1;45;34m$i\e[0m" 
                sleep 1
        done
        echo -e  "please singer \e[1;31m${singer[lucky_singer]}\e[0m sing for us \e[1;34m${songs[lucky_song]}\e[0m" 
        unset singer[lucky_singer]
        echo ${singer[@]} >singer.txt
        unset songs[lucky_song]
        echo ${songs[@]} >songs.txt
        num=`cat singer.txt |wc -w`
        echo -e "\e[46;31m no singing singers are: ${singer[@]}\e[0m"
        echo -e "\e[46;31m no singing songs are: ${songs[@]}\e[0m"
        if (( num==0 ));then
                read -p "now all singer already sang,again" again
                cp singer_ex.txt  singer.txt
                read -p "now songs will restart!"
                cp songs_ex.txt songs.txt
        fi                                                   
}

#3.add singer 
add_singer(){
	read -p "please input others singers:" u_others
	echo "${u_others}" >>singer_ex.txt 
}

#4.add songs
add_songs(){
	read -p "please input others songs:" u_song
	echo "${u_song}" >>songs_ex.txt 
}

#main list
main(){
	while :
	do
	menu
	case $u_opt in
	1)
		display_singer_somgs
		;;
	2)
		choice_song
		;;
	3)
		add_singer
		;;
	4)
		add_songs
		;;
	5)
		exit
		;;
	*)
		exit
		;; 
	esac
	done
	read -p "please press enter to continue"
}
main
