#!/bin/bash
#clear file
>scan_ftp_up.txt
>scan_ftp_down.txt
>scan_mysql_up.txt
>scan_mysql_down.txt
>scan_web_up.txt
>scan_web_down.txt
#scan
for i in {1..254}
do
	#ftp scan
	(nc -z 192.168.0.$i 21 &>/dev/null
	if (($?==0));then
		echo "192.168.0.$i" >>scan_ftp_up.txt
	else 
		echo "192.168.0.$i" >>scan_ftp_down.txt
	fi)&
	#mysql scan
	(nc -z 192.168.0.$i 3306 &>/dev/null
        if (($?==0));then
                echo "192.168.0.$i" >>scan_mysql_up.txt
        else    
                echo "192.168.0.$i" >>scan_mysql_down.txt
        fi)&
	#web scan
	(nc -z 192.168.0.$i 80 &>/dev/null
        if (($?==0));then
                echo "192.168.0.$i" >>scan_web_up.txt
        else    
                echo "192.168.0.$i" >>scan_web_down.txt
        fi)&	

done
sleep 4
#ftp result
ftp_up=`cat scan_ftp_up.txt`
#ftp_down=`cat scan_ftp_down.txt`
ftp_up_total=`cat scan_ftp_up.txt |wc -l`
#ftp_down_total=`cat scan_ftp_down.txt |wc -l`
#mysql result
mysql_up=`cat scan_mysql_up.txt`
#mysql_down=`cat scan_mysql_down.txt`
mysql_up_total=`cat scan_mysql_up.txt |wc -l`
#mysql_down_total=`cat scan_mysql_down.txt |wc -l`
#web
web_up=`cat scan_web_up.txt`
#web_down=`cat scan_web_down.txt`
web_up_total=`cat scan_web_up.txt |wc -l`
#web_down_total=`cat scan_web_down.txt |wc -l`
#show
echo "========================================"
echo "ftp server up pc total ${ftp_up_total}"
echo "${ftp_up}"
echo "========================================"
echo "mysql server up pc total ${mysql_up_total}"
echo "${mysql_up}"
echo "========================================"
echo "web server up pc total ${web_up_total}"
echo "${web_up}"
