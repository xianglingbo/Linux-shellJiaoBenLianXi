#!/bin/bash
#disk  name  partition number

accept_info(){
echo  -e "\033[32m  please input your disk name \033[0m "
read  disk_name
}

fdisk_fun(){
	fdisk  /dev/$disk_name <<EOF &>/dev/null 
n
p
1

+10G
n
p
2

+20G
n
p
3


w
EOF
fdisk -l /dev/$disk_name	
}


menu(){

	echo  "1.select disk info"
	echo  "2.manual disk "
	echo  "3.exit"
	read   -p "please key in your choose option:"  choose_option
}
disk_info(){
	fdisk -l /dev/$disk_name
}
mkfs_fun(){
	for i in {1..3}
	do
	mkfs.ext4 /dev/$disk_name$i
	done
}

auto_mount(){
	for i in {1..3}
	do
	echo "please key in mount point directory path"
	read point
	[ -d $point ] &&echo point dir is exits||mkdir $point
	echo "/dev/$disk_name$i  $point ext4 defaults 0 0" >>/etc/fstab
	done	
	cat /etc/fstab
	mount -a
	df -Th
}

main(){
	clear
	while true
	do
		menu
	case "$choose_option" in
	1)
		accept_info
		disk_info
		;;
	2)
		accept_info
		fdisk_fun
		mkfs_fun
		auto_mount
		;;
	3)
		exit
		;; 
	*)
		exit;;
	esac
	read -n 1 -p "please press any key contine  "  contine_key
	clear
	done
	
}
main

