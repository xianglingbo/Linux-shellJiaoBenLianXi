#!/bin/bash
#filename:yumfeng.sh
#author:xlb

[ -d /yum ] || mkdir /yum
df |grep "yum$" &>/dev/null || mount /dev/cdrom /yum
cd /etc/yum.repos.d
cat >localyum.repo <<EOF
[local_yum]
name=localyum
baseurl=file:///yum
enabled=1
gpgcheck=0
EOF
[ -d bat ] || (mkdir bat; mv C* bat)
yum clean all
yum list &>/dev/null
if (($?==0));then
	echo "local yum build success"
else 
	echo "local yum build failed"
fi 
