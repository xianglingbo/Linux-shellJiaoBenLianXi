#!/bin/bash
#filename:query.sh

read -p "input group name:" groupname
cat /etc/group |egrep $groupname >/dev/null
if [ $? -eq 0 ];then
	groupid=`cat /etc/group |egrep $groupname |awk -F: '{print $3}'`
	username=`cat /etc/passwd |egrep $groupid |awk -F: '{print $1}'`
	echo -e "\t${groupname} groupid is \e[31m${groupid}\e[0m"
	echo -e "\t\e[36m${groupname} group include users: \e[0m \n\e[35m${username} \e[0m"
else 
	echo -e "\t\e[31mthis groupname don't exist!\e[0m"
fi
