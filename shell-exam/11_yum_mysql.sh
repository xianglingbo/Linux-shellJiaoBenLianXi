#!/bin/bash

#yum config
#1.mount
dir="/yum"
[ -d ${dir} ] && echo "the /yum is already" || mkdir /yum
mount /dev/cdrom ${dir}
if [[ $? == 0 ]];then
	echo "the CD mount success"
else
	echo "the CD already mount"
fi
#2.write repo file
repo="/etc/yum.repos.d/Cen*"
local="/etc/yum.repos.d/localyum.repo"
if [ -f ${local} ];then
	echo "the localyum.repo is exist"
else
cat >${local}  <<EOF
[localyum]
name=local_yum
baseurl=file:///yum
enabled=1
gpgcheck=0
EOF
[ $? -eq 0 ] && echo "write localyum.repr success" || echo "write localyum.repo is failed"
#3.move others repos files
[ -d /etc/yum.repos.d/backup ] && echo "the backup is created" || mkdir /etc/yum.repos.d/backup
mv ${repo} /etc/yum.repos.d/backup/
#4.test repotory
yum repolist
[ $? -eq 0 ]  && echo "ok,you use localyum" || echo "can't use localyum,have worry"

#mysql
yum install mysql mysql-server -y
service mysqld start
read -s -p "please input password for mysql root user:" u_pwd
mysqladmin -uroot password "${u_pwd}"
service mysqld restart
chkconfig mysqld on
cat >/etc/rc.local <<EOF
service mysqld start
EOF


